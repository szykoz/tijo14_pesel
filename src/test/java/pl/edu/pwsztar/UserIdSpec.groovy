package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check date for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.date.orElse(null) == expectedDate
        where:
            id                  | expectedDate
            '76030146492'       | '01-03-1976'
            '83101878369'       | '18-10-1983'
            '06261194353'       | '11-06-2006'
            '95101578331'       | null
            'abdtujnopff'       | null
    }
}
